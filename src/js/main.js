$(function() {

    (function quantityProducts() {
        var $quantityArrowMinus = $(".quantity-arrow-minus");
        var $quantityArrowPlus = $(".quantity-arrow-plus");
        var $quantityNum = $(".quantity-num");

        $quantityArrowMinus.click(quantityMinus);
        $quantityArrowPlus.click(quantityPlus);

        function quantityMinus() {
            if ($quantityNum.val() > 1) {
                $quantityNum.val(+$quantityNum.val() - 1);
            }
        }

        function quantityPlus() {
            $quantityNum.val(+$quantityNum.val() + 1);
        }
    })();

});

// var swiper = new Swiper('.swiper-container');
//
$(document).ready(function () {
    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'vertical',
        loop: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        // pagination: {
        //     el: '.swiper-pagination',
        //     clickable: true,
        //     renderBullet: function (index, className) {
        //         return '<span class="' + className + '">' + (index + 1) + '</span>';
        //     },
        // },
        // thumbs: {
        //     swiper: thumbsSwiper
        // },

    })
});

// $(document).ready(function () {
//     var Swiper = Swiper('.gallery-thumbs', {
//         // Optional parameters
//         // direction: 'vertical',
//     direction: 'horizontal',
// })
// });
// var mySwiper = document.querySelector('.swiper-container').swiper.mySwiper.slideNext();
//fixed menu
$(document).ready(function () {

    var $menu = $("#menu");

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100 && $menu.hasClass("default")) {
            $menu.removeClass("default").addClass("fixed");
        } else if ($(this).scrollTop() <= 100 && $menu.hasClass("fixed")) {
            $menu.removeClass("fixed").addClass("default");
        }
    });//scroll
});

//click
$(".dropdown-toggle").mouseenter(function () {
    $(this).click()
});
$(".header__first").mouseenter(function () {
    $(this).click();
    $(this).click()
});

//slider
$(document).ready(function () {
    let $rangers = $("#slider-range");

    $rangers.slider({
        range: true,
        min: 0,
        max: 200000,
        values: [1050, 200000],
        slide: function (event, ui) {
            $("#tip-from").val(ui.values[0] + "₽");
            $("#tip-before").val(ui.values[1] + "₽");
        }
    });

    $rangers.val($("#slider-range").slider("values", 0) + "₽" +
        " - " + $("#slider-range").slider("values", 1) + "₽");

    $("#tip-from").val($rangers.slider("values", 0) + "₽");
    $("#tip-before").val($rangers.slider("values", 1) + "₽");

// --from
    let thumb = $($rangers.children('.ui-slider-handle'));
    setLabelPosition();

    $rangers.bind('slide', function () {
        $('#tip-from').val($rangers.slider('value'));
        setLabelPosition();
    });

    function setLabelPosition() {
        let labelFrom = $('#tip-from');
        labelFrom.css('bottom', thumb.offset().bottom + (labelFrom.outerHeight() + thumb.width()) / 1);
        labelFrom.css('left', thumb.offset().left - (labelFrom.width() - thumb.width()) / 5);
    }

// --before
    let thumbBefore = $($rangers.children('.ui-slider-handle:last-child'));
    setLabelPositionBefore();

    $rangers.bind('slide', function () {
        $('#tip-before').val($rangers.slider('value'));
        setLabelPositionBefore();
    });

    function setLabelPositionBefore() {
        let labelBefore = $('#tip-before');
        labelBefore.css('top', thumbBefore.offset().top + labelBefore.outerHeight(true));
        labelBefore.css('left', thumbBefore.offset().left - (labelBefore.width() - thumbBefore.width()) / 2);
    }

})
;
//End of slider

// Nav
// $('.nav__item').mouseenter(function () {
//     if (!$(this).hasClass('show')) {
//         $('.dropdown-toggle', this).trigger('click');
//     }
// });
//Hiding items
let $filterAside = $('.aside-left__item'),
    $head = $filterAside.children('.aside-left__item--title');

$head.on('click', function () {
    $(this).toggleClass('aside-left__item--closed');
    $(this).toggleClass('disabled');
});
//
let $hideFilters = $('.icon-icon-filter');
$hideFilters.on('click', function () {
    $('.aside-left').toggleClass('hide');
    $('.catalog__list').toggleClass('fullscreen');
    $(this).toggleClass('turned-off');
    $('.section-filters__title').toggleClass('bbtransparent');
});

//***
let $searchClosed = $('.menu__search');
$searchClosed.on('mouseover', function () {
    $(".menu__search").children(".search").css({"opacity": "1", "width": "15rem"});
});
// ,"min-width":"2rem","width":"inherit"
$searchClosed.on('mouseout', function () {
    $(".menu__search").children(".search").css({"opacity": "0", "width": "0rem"});
});


